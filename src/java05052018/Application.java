package java05052018;
import java.util.Random;
import java.util.Scanner;
public class Application {
    private int[] array=new int[0];
    public static void main(String[] args){
        new Application().runTheApp();
    }
     void runTheApp(){
         readTheUserInput();
    }
     void showMenu(){
        System.out.println("0-выход");
        System.out.println("1-инициальизация массива");
        System.out.println("2-заполнение массива");
        System.out.println("3-сорт по возрастанию");
        System.out.println("4-сорт по убыванию");
        System.out.println("5-вывод элементов массива");
    }
    int prcessUserChoice(){
        Scanner s= new Scanner(System.in);
        if(s.hasNextInt()==true) {
            return s.nextInt();
        }
        System.out.println("так не пойдет");
        System.out.println("попробуй снова");
        return prcessUserChoice();
    }
    void readTheUserInput(){
        for(int i=0;i>=0;) {
            showMenu();
            switch (prcessUserChoice()) {
                case 0:
                    System.out.println("sayonara!");
                    System.exit(0);
                case 1:
                    createAnArrey();
                    break;
                case 2:
                    if(array.length==0) {
                        System.out.println("сперва:");
                        createAnArrey();
                    }
                    fillAnArrey();
                    break;
                case 3:
                    sortAnArrey(true);
                    break;
                case 4:
                    sortAnArrey(false);
                    break;
                case 5:
                    printAnElement();
                    break;
                default:
                    System.out.println("мой ответ- 42! шутка, великий думатель просит попробовать снова)");
                    break;
            }
        }
    }
    void createAnArrey(){
        System.out.println("введите размер массива");
        array = new int[prcessUserChoice()];
    }
    void fillAnArrey(){
        System.out.println("1-заполнение массива случайными элементами");
        System.out.println("2-пользовательский ввод");
        switch (prcessUserChoice()) {
            case 1:
                Random rud =new Random(System.currentTimeMillis());
                for(int i=0;i<array.length;i++){
                    array[i]=rud.nextInt(20);
                }
                break;
            case 2:
                for(int i=0;i<array.length;i++){
                    array[i]=prcessUserChoice();
                }
            default:
                System.out.println("попробуй еще");
                fillAnArrey();
                break;
        }
    }
    void sortAnArrey(boolean ase){
        if(ase==true){
            int a;
            int b;
            for(int i=0;i<array.length;i++){
                for(int j=1;j<array.length-i;j++){
                    a=array[j-1];
                    b=array[j];
                    if(a>b){
                        array[j-1]=b;
                        array[j]=a;
                    }
                }
            }
        }
        if(ase==false){
            int a;
            int b;
            for(int i=0;i<array.length;i++){
                for(int j=1;j<array.length-i;j++){
                    a=array[j-1];
                    b=array[j];
                    if(a<b){
                        array[j-1]=b;
                        array[j]=a;
                }
                }
            }
        }
    }
    void printAnElement(){
        int ind=0;
        while(ind<array.length) {
            System.out.print(array[ind++] + " ");
        }
        System.out.println();
    }

}
