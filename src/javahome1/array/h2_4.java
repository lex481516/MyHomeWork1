package javahome1.array;
import java.util.Scanner;
import java.util.Random;
public class h2_4 {
    public static void main(String[] args){
        Scanner numm = new Scanner(System.in);
        Random randi = new Random(System.currentTimeMillis());
        int size =numm.nextInt();
        int[] arry = new int[size];
        System.out.println("исходный массив");
        for(int i=0;i<arry.length;i++){
            arry[i]=randi.nextInt(100);
            System.out.print(arry[i]+" ");
        }
        System.out.println();
        System.out.println("шейкерная сортировка");
        int a=0;
        int b=0;
        int n=1;
        for(int i=0;i<arry.length-n;i++){
                a=arry[i];
                b=arry[i+1];
                if(a>b){
                    arry[i]=b;
                    arry[i+1]=a;
                }
                if(i==arry.length-1-n){
                    for(int j=arry.length-n;j>n-1;j--){
                        a=arry[j-1];
                        b=arry[j];
                        if(a>b){
                            arry[j-1]=b;
                            arry[j]=a;
                        }
                    }
                    n++;
                    i=n;
                }
                if(n==arry.length){
                    break;
                }

        }
        System.out.println();
        System.out.println("новый массив:");
        for(int c:arry){
            System.out.print(c+" ");
        }
    }

}