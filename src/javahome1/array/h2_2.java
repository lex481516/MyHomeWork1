package javahome1.array;
import java.util.Scanner;
import java.util.Random;
public class h2_2 {
    public static void main(String[] args){
        Scanner numm = new Scanner(System.in);
        Random randi = new Random(System.currentTimeMillis());
        int size =numm.nextInt();
        int[] arry = new int[size];
        System.out.println("исходный массив");
        for(int i=0;i<arry.length;i++){
            arry[i]=randi.nextInt(100);
            System.out.print(arry[i]+" ");
        }
        //глупая сортировка
        System.out.println();
        System.out.println("глупая сортировка:");
        int a=0,b=0;
        for(int i=0;i<arry.length-1;i++){
            a=arry[i];
            b=arry[i+1];
            if(a>b){
                arry[i]=b;
                arry[i+1]=a;
                i=-1;
            }
        }
        System.out.println();
        System.out.println("новый массив:");
        for(int i:arry){
            System.out.print(i+" ");
        }
    }
}
