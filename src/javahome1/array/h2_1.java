package javahome1.array;
import java.util.Random;
import java.util.Scanner;
public class h2_1 {
    public static void main(String[] args){
        //рандом
        Random rundi = new Random(System.currentTimeMillis());
        //задание колличества 2D массива
        Scanner num = new Scanner(System.in);
        //создание и цикл инициализации 2D массива
        int[][] arry = new int[num.nextInt()][];
        for(int i=0;i<arry.length;i++){
            arry[i]= new int[rundi.nextInt(100)];
            for(int j=0;j<arry[i].length;j++){
                arry[i][j]=rundi.nextInt(100);
            }
        }
        //цикл для вывода элементов массива
        for(int[] arry2: arry){
            for(int i: arry2){
                System.out.print(i+" ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println();
        //вывывод самого длинного массива
        int size=arry[0].length;
        for(int i=1;i<arry.length;i++){
            if(size<arry[i].length){
                size=arry[i].length;
            }
        }
        System.out.println("саммые длинные 1D массивы:");
        for(int i=0;i<arry.length;i++) {
            if(arry[i].length==size) {
                for(int j: arry[i]) {
                    System.out.print(j + " ");
                }
                System.out.println();
            }
        }
        System.out.println();
        System.out.println("сумма их элементов:");
        int sum1=0;
        for(int i=0;i<arry.length;i++) {
            if(arry[i].length==size) {
                for(int j: arry[i]) {
                    sum1 += j;
                }
            }
        }
        System.out.println(sum1);
        //вывод самого короткого массива
        size=arry[0].length;
        for(int i=0;i<arry.length;i++){
            if(size>arry[i].length){
                size=arry[i].length;
            }
        }
        System.out.println("Саммые короткиие 1D массивы:");
        for(int i=0;i<arry.length;i++) {
            if(arry[i].length==size) {
                for(int j: arry[i]) {
                    System.out.print(j + " ");
                }
                System.out.println();
            }
        }
        System.out.println();
        System.out.println("сумма их эллементов");
        int sum2=0;
        for(int i=0;i<arry.length;i++) {
            if(arry[i].length==size) {
                for(int j: arry[i]) {
                    sum2 += j;
                }
            }
        }
        System.out.println(sum2);
        System.out.println();
        System.out.println();
        System.out.println();
        //вывод суммы элементов массивов с четнми индексами
        System.out.println("3 задание(как я его понял)");
        System.out.println("сумма элементов четных массивов");
        int sum3=0;
        for(int i=0;i<arry.length;i++){
            int chet = i%2;
            if(chet==0){
                for(int el: arry[i]){
                    sum3+=el;
                }
            }
        }
        //вывод суммы элементов массивов с четным колличеством элементов
        System.out.println(sum3);
        System.out.println("сумма элементов массивов с четным колличеством элементов");
        int sum4=0;
        for(int i=0;i<arry.length;i++){
            int chet = arry[i].length%2;
            if(chet==0){
                for(int el: arry[i]){
                    sum4+=el;
                }
            }
        }
        System.out.println(sum4);
    }
}
