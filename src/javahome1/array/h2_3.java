package javahome1.array;
import java.util.Scanner;
import java.util.Random;
public class h2_3 {
    public static void main(String[] args){
        Scanner numm = new Scanner(System.in);
        Random randi = new Random(System.currentTimeMillis());
        int size =numm.nextInt();
        int[] arry = new int[size];
        System.out.println("исходный массив");
        for(int i=0;i<arry.length;i++){
            arry[i]=randi.nextInt(100);
            System.out.print(arry[i]+" ");
        }
        System.out.println();
        System.out.println("пузырбковая сортировка");
        int a=0;
        int b=0;
        for(int i=0;i<arry.length;i++){
            for(int j=1;j<arry.length-i;j++){
                a=arry[j-1];
                b=arry[j];
                if(a>b){
                    arry[j-1]=b;
                    arry[j]=a;
                }
            }
        }
        System.out.println();
        System.out.println("новый массив:");
        for(int c:arry){
            System.out.print(c+" ");
        }
    }

}
